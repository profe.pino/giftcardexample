/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appgiftcard;
import java.util.Scanner;
/**
 *
 * @author jpino
 */
public class AppGiftCard {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner teclado = new Scanner(System.in);
        int opcion;
        Trabajador trabajador = new Trabajador();
        String nombre="";
        String run="";
        do {
            System.out.println("-----------------------------");
            System.out.println("            MENU");
            System.out.println("-----------------------------");
            System.out.println(" 1. Crear Trabajador");
            System.out.println(" 2. Ver Trabajador");
            System.out.println(" 3. Crear GiftCard");
            System.out.println(" 4. Ver GiftCard  ");
            System.out.println(" 5. Verificar Tarjeta");
            System.out.println(" 6. Comprar");
            System.out.println(" 7. Salir");
            System.out.println("-----------------------------");
            System.out.print(" Opcion: ");
            opcion = teclado.nextInt();
            if (opcion < 1 || opcion >7)
                System.out.println("Ingrese una opcion valida!!!");
            else {
                switch (opcion) {
                    case 1: //Crear trabajador
                            do {
                                nombre ="";
                                System.out.print("Nombre: ");
                                nombre = teclado.next();
                            } while (nombre.trim().length()==0);
                            do {
                                System.out.print("Run   : ");
                                run=teclado.next();
                            } while (run.trim().length()==0);
                            if (nombre.trim().length()>0 && 
                                run.trim().length()>=9) {
                                trabajador.setNombre(nombre);
                                trabajador.setRun(run.substring(0, 
                                            run.length()-2));
                                trabajador.setDv(run.charAt(run.length()-1));
                            }
                            break;
                    case 2: if (trabajador!=null) 
                                trabajador.ver();
                            else
                                System.out.println("Trabajador no existe!");
                            break;
                    case 3: //Crear Giftcard
                            break;
                    case 4: //Ver giftcard
                            break;
                    case 5://Verificar giftcard
                            break;
                    case 6: //Comprar
                            break;
                    case 7: //Salir 
                            System.out.println("Hasta la vista!!!");
                }
            }
            
        } while (opcion!=7);
        
        
        
    }
    
}
