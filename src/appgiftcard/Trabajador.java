/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appgiftcard;

/**
 *
 * @author jpino
 */
public class Trabajador {
    private String run;
    private char dv;
    private String nombre;
    //Constructor por defecto
    public Trabajador() {
    }
    //Constructor ccon parametros
    public Trabajador(String run, char dv, String nombre) {
        this.run = run;
        this.dv = dv;
        this.nombre = nombre;
    }
    //SET - GET

    public String getRun() {
        return run;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public char getDv() {
        return dv;
    }

    public void setDv(char dv) {
        this.dv = dv;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    // Metodo Customer
    public void ver() {
        System.out.println("--------------------------------");
        System.out.println("         TRABAJADOR ");
        System.out.println("--------------------------------");
        System.out.println(" Nombre : "+this.nombre);
        System.out.println(" Run    : "+this.run);
        System.out.println(" DV     : "+this.dv);
        System.out.println("--------------------------------");
    }
    
   
    
}
